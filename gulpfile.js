const wordpress_theme = 'wordpress_theme_here';
const theme_path = 'wordpress/wp-content/themes/'+wordpress_theme;

const { src, dest, watch, series, parallel } = require('gulp');

const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const cssnano = require('cssnano');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const browserSync = require('browser-sync').create();

const onError = function (err) {
    notify.onError({
        title:    "Gulp",
        subtitle: "Failure!",
        message:  "Error: <%= error.message %>",
        sound:    "Beep"
    })(err);
    this.emit('end');
};

// File paths
const files = { 
    cssPath: theme_path+'/assets/scss/**/*.*ss',
    jsHeaderPath: theme_path+'/assets/js/modules/*.js',
    jsPath: theme_path+'/assets/js/partials/*.js',
    adminJsPath: theme_path+'/assets/js/admin/*.js'
}

// normal css
function css() {
    return src(theme_path+'/assets/scss/style.scss')
        .pipe(plumber({errorHandler: onError}))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer(),cssnano()], {syntax:require('postcss-scss')}))
        .pipe(sourcemaps.write())
        .pipe(dest(theme_path+'/dist/css'))
        .pipe(notify('SCSS! (っ◕‿◕)っ'))
        .pipe(browserSync.stream());
}

// admin css
function cssAdmin() {
    return src(theme_path+'/assets/scss/admin-styles.scss')
        .pipe(plumber({errorHandler: onError}))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer(),cssnano()], {syntax:require('postcss-scss')}))
        .pipe(sourcemaps.write())
        .pipe(dest(theme_path+'/dist/css'))
        .pipe(notify('Admin SCSS! ᕦ(ˇò_ó)ᕤ'));
}

// editor css
function cssEditor() {
    return src(theme_path+'/assets/scss/style-editor.scss')
        .pipe(plumber({errorHandler: onError}))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer(),cssnano()], {syntax:require('postcss-scss')}))
        .pipe(sourcemaps.write())
        .pipe(dest(theme_path))
        .pipe(notify('Editor SCSS! ᕦ(ˇò_ó)ᕤ'));
}

// js
function js() {
    return src(files.jsPath, { sourcemaps: true })
        .pipe(plumber({errorHandler: onError}))
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(dest(theme_path+'/dist/js'), { sourcemaps: true })
        .pipe(notify('JavaScript Footer! (⌐■_■)'))
        .pipe(browserSync.stream());
}

// js header
function jsHeader() {
    return src(files.jsHeaderPath, { sourcemaps: true })
        .pipe(plumber({errorHandler: onError}))
        .pipe(concat('header-scripts.min.js'))
        .pipe(uglify())
        .pipe(dest(theme_path+'/dist/js'), { sourcemaps: true })
        .pipe(notify('JavaScript Header! (⌐□_□)'));
}

// js admin
function jsAdmin() {
    return src(files.adminJsPath, { sourcemaps: true })
        .pipe(plumber({errorHandler: onError}))
        .pipe(concat('admin-scripts.min.js'))
        .pipe(uglify())
        .pipe(dest(theme_path+'/dist/js'), { sourcemaps: true })
        .pipe(notify('Admin JavaScript! (づ￣ ³￣)づ'));
}

function watchTask(){
    browserSync.init({
        files: [
            theme_path+'/*.php'
        ],
        proxy: 'http://wordpress_url_here/',
        host: 'wordpress_url_here',
        open: 'external',
    });

    watch(files.cssPath,series(css));
    watch(files.cssPath,series(cssAdmin));
    watch(files.cssPath,series(cssEditor));
    watch(files.jsHeaderPath,series(jsHeader));
    watch(files.jsPath,series(js));
    watch(files.adminJsPath,series(jsAdmin)); 
}

exports.default = watchTask;