#!/bin/bash

site=$1
theme=$2
url=$3

# ==================================
# =============  KILL DEFAULT THEMES
# ==================================
find . -type d -regex ".*\/wp-content\/themes\/twenty.*" -exec rm -rf {} \;


# ==================================
# ===================  EDIT GULPFILE
# ==================================
sed -i ".bak" "s/wordpress_theme_here/$theme/g" gulpfile.js
sed -i ".bak" "s/wordpress_url_here/$url/g" gulpfile.js
rm gulpfile.js.bak


# ==================================
# ==================  EDIT WP-CONFIG
# ==================================
sed -i ".bak" "s/database_name_here/$theme/g" wordpress/wp-config-sample.php
sed -i ".bak" "s/username_here/root/g" wordpress/wp-config-sample.php
sed -i ".bak" "s/password_here/root/g" wordpress/wp-config-sample.php
# sed -i ".bak" "s/'WP_DEBUG', false/'WP_DEBUG', true/g" wordpress/wp-config-sample.php
rm wordpress/wp-config-sample.php.bak
mv wordpress/wp-config-sample.php wordpress/wp-config.php
# echo "
# if(WP_DEBUG):
#     // Enable Debug logging to the /wp-content/debug.log file
#     define('WP_DEBUG_LOG', true);

#     // Disable display of errors and warnings
#     define('WP_DEBUG_DISPLAY', false);
#     @ini_set('display_errors',0);
# endif;
# " >> wordpress/wp-config.php


# ==================================
# ===============  CREATE GIT IGNORE
# ==================================
echo -n "# This is a template .gitignore file for git-managed WordPress projects.
#
# Stick this file up your repository root (which it assumes is
# also the WordPress root directory) and add exceptions for any plugins,
# themes, and other directories that should be under version control.
#
# See git's documentation for more info on .gitignore files:
# http://kernel.org/pub/software/scm/git/docs/gitignore.html

# Ignore everything in the root except the \"wp-content\" directory.

/*
!.gitignore
*.sass-cache*

# Ignore everything in the \"wp-content\" directory, except the \"plugins\" and \"themes\" directories.

!plugins/
!themes/

# Ignore everything in the \"themes\" directory, except the themes you specify (see the commented-out example)

themes/*
!themes/$theme/

# Ignore everything in the \"plugins\" directory, except the plugins you specify (see the commented-out example)

plugins/*" > wordpress/wp-content/.gitignore


# ==================================
# ===  RENAME SITE AND THEME FOLDERS
# ==================================
mv ../wordpress-installer ../$site
mv ../$site/wordpress/wp-content/themes/wordpress-base-theme ../$site/wordpress/wp-content/themes/$theme


# ==================================
# =================  CREATE DATABASE
# ==================================
/Applications/MAMP/Library/bin/mysql -uroot -proot -e "create database  $theme;"

# ==================================
# ================  CREATE MAMP SITE
# ==================================
#/Applications/MAMP\ PRO.app/Contents/MacOS/MAMP\ PRO cmd createHost $url /Documents/htdocs/$site