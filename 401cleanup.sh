#!/bin/bash

# ==================================
# =================  KILL BASE THEME
# ==================================
find . -type d -regex ".*\/wp-content\/themes\/wordpress-base.*" -exec rm -rf {} \;
echo 'Removed WordPress Base Theme';